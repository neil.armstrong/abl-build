#!/bin/bash

set -xe

ABL_DIR=$(readlink -f $1)
CLANG_DIR=$(readlink -f $2)
OUT_DIR=$3
TARGET=$4

[ -f $OUT_DIR ] || mkdir -p $OUT_DIR

OUT_DIR=$(readlink -f $OUT_DIR)

ROOT_DIR=$ABL_DIR
ABL_SRC=""

export TARGET_BUILD_VARIANT="userdebug"

ABL_OUT_DIR="${OUT_DIR}/abl-$TARGET-${TARGET_BUILD_VARIANT}"
ABL_IMAGE_NAME="abl_${TARGET_BUILD_VARIANT}.elf"
ABL_IMAGE_DIR=${ABL_OUT_DIR}
mkdir -p "${ABL_IMAGE_DIR}"

source "${ROOT_DIR}/QcomModulePkg/build.config.msm.$TARGET"

PREBUILT_HOST_TOOLS="BUILD_CC=clang BUILD_CXX=clang++ LDPATH=-fuse-ld=lld BUILD_AR=llvm-ar"

export PATH=${CLANG_DIR}:$PATH

MKABL_ARGS=("-C" "${ROOT_DIR}/${ABL_SRC}")
MKABL_ARGS+=("BOOTLOADER_OUT=${ABL_OUT_DIR}/obj/ABL_OUT" "all")
MKABL_ARGS+=("PREBUILT_HOST_TOOLS=${PREBUILT_HOST_TOOLS}")
MKABL_ARGS+=("${MAKE_FLAGS[@]}")
MKABL_ARGS+=("CLANG_BIN=${CLANG_DIR}/")

make "${MKABL_ARGS[@]}"

ABL_DEBUG_FILE="$(find "${ABL_OUT_DIR}" -name LinuxLoader.debug)"
if [ -e "${ABL_DEBUG_FILE}" ]; then
	cp "${ABL_DEBUG_FILE}" "${ABL_IMAGE_DIR}/LinuxLoader_${TARGET_BUILD_VARIANT}.debug"
	cp "${ABL_OUT_DIR}/unsigned_abl.elf" "${ABL_IMAGE_DIR}/unsigned_abl_${TARGET_BUILD_VARIANT}.elf"
fi
