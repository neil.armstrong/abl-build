# Build ABL

```
$ git submodules init
$ git submodule update
```

## SM8550

```
$ ./build.sh $PWD/edk2 $PWD/linux-x86/clang-r450784e/bin/ $PWD/out kalama
```

Sign with sectools:
```
$ ./sign.sh $PWD/edk2 /path/to/sectools /path/to/profile.xml out kalama
```

or:

```
$ /path/to/mbn-sign out/abl-kalama-userdebug/unsigned_abl.elf out/abl-kalama-userdebug/abl_userdebug.elf abl /path/to/config/sm8550.toml
```

then:

```
$ fastboot flash abl out/abl-kalama-userdebug/abl_userdebug.elf
```


## SM8650

```
$ ./build.sh $PWD/edk2 $PWD/linux-x86/clang-r450784e/bin/ $PWD/out pineapple
```

Sign with sectools:
```
$ ./sign.sh $PWD/edk2 /path/to/sectools /path/to/profile.xml out pineapple
```

or:

```
$ /path/to/mbn-sign out/abl-pineapple-userdebug/unsigned_abl.elf out/abl-pineapple-userdebug/abl_userdebug.elf abl /path/to/config/sm8650.toml
```

then:

```
$ fastboot flash abl out/abl-pineapple-userdebug/abl_userdebug.elf
```
